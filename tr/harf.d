module tr.harf;

import std.string;

/**
 * Tek bir alfabe harfini temsil eder
 */
struct Harf
{
    dchar kod;

    string toString() const
    {
        return format("%s", kod);
    }
}

unittest
{
    import std.conv;

    auto h = Harf('ğ');
    assert(to!string(h) == "ğ");
}
