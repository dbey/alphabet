/**
 * Bu modül belirli bir alfabeye bağlı olan karakterlerle ilgilidir
 */

module tr.im;

import tr.alfabe;
import std.conv;
import std.traits;

debug(tr_im) import std.stdio;

/**
 * Belirli bir alfabeye bağlı bir "im"i temsil eder
 *
 * Alfabedeki harflere ek olarak herhangi bir Unicode karakterini de temsil
 * edebilir. Alfabenin harfleri her zaman için en başta sıralanırlar.
 *
 * Harflerin küçük-büyük dönüşümleri ve sıra karşılaştırmaları alfabenin
 * kurallarına göre yapılır.
 */
struct İm(string kod)
{
private:

    dchar karakter_;

public:

    this(T)(T karakter)
        if (isSomeChar!T)
    {
        karakter_ = karakter;
    }

    this(T)(T im)
        if (is (T : İm))
    {
        karakter_ = im.karakter_;
    }

    void opAssign(T)(T karakter)
        if (isSomeChar!T)
    {
        karakter_ = karakter;
    }

    void opAssign(T)(T im)
        if (is(T : İm))
    {
        karakter_ = im.karakter_;
    }

    /**
     * Karakteri char, wchar, dchar, veya kendi türüyle karşılaştırır
     *
     * Farklı alfabeye ait İm'ler karşılaştırılamazlar.
     * Karşılaştırılabilmeleri için bir veya ikisinin dchar'a
     * dönüştürülmesi gerekir.
     */
    bool opEquals(T)(in T sağdaki) const
        if (isSomeChar!T)
    {
            return karakter_ == sağdaki;
    }
    bool opEquals(T)(in T sağdaki) const
        if (is( T : İm))
    {
            return karakter_ == sağdaki.karakter_;
    }

    /**
     * İmleri alfabenin kurallarına göre karşılaştırır
     *
     * Önce temel farklılığa bakar, sonra aksanlarına, en son da
     * küçük/büyük ayrımına
     */
    int opCmp(T)(in T sağdaki) const
    {
        const Sıra sıra = sıra(sağdaki);

        return üçlüSıraKarşılaştırması(sıra.temel,
                                       sıra.aksan,
                                       sıra.küçükBüyük);
    }

    /**
     * dchar dönüşümü
     */
    dchar opCast(Tür : dchar)() const
    {
        return karakter_;
    }

    /**
     * İmin yazı içinde kullanıldığı durumlardaki sıralanması için
     * kullanılan sıra bilgisini döndürür
     *
     * Örneğin 'a' 'â'dan önce olduğu halde "hâl"in "ham"dan önce
     * sıralanabilmesi için yararlıdır
     */
    Sıra sıra(dchar sağdaki) const
    {
        return alfabe!kod.sıra(karakter_, sağdaki);
    }

    /** ditto */
    Sıra sıra(İm!kod sağdaki) const
    {
        return alfabe!kod.sıra(karakter_, sağdaki.karakter_);
    }

    /**
     * Küçüğünü döndürür
     */
    @property İm!kod küçüğü() const
    {
        return İm!kod(alfabe!kod.küçüğü(karakter_));
    }

    /**
     * Büyüğünü döndürür
     */
    @property İm!kod büyüğü() const
    {
        return İm!kod(alfabe!kod.büyüğü(karakter_));
    }

    /**
     * Aksansız olanını döndürür
     */
    @property İm!kod aksansızı() const
    {
        return İm!kod(alfabe!kod.aksansızı(karakter_));
    }

    string toString() const
    {
        return to!string(karakter_);
    }
}

unittest
{
    import std.traits;

    alias İm!"tur" im_tr;

    {
        im_tr k;
        assert(k.karakter_ == k.karakter_.init);
    }
    {
        auto k = im_tr('a');
        assert(k.karakter_ == 'a');

        k = 'ğ';
        assert(k.karakter_ == 'ğ');

        k = im_tr('X');
        assert(k.karakter_ == 'X');
    }
    {
        char c = 'b';
        auto k = im_tr(c);
        assert(k.karakter_ == 'b');
    }
    {
        auto k = im_tr('ğ');
        assert(k.karakter_ == 'ğ');
    }
    {
        wchar c = 'ğ';
        auto k = im_tr(c);
        assert(k.karakter_ == 'ğ');
    }
    {
        auto k = im_tr('\U0001ffff');
        assert(k.karakter_ == '\U0001ffff');
    }
    {
        dchar c = '\U0001ffff';
        auto k = im_tr(c);
        assert(k.karakter_ == '\U0001ffff');
    }
    assert(im_tr('ı').toString == "ı");

    assert(im_tr('c') == 'c');
    assert(im_tr('ş') == 'ş');
    assert(im_tr('\U0001ffff') == '\U0001ffff');

    assert('a' == im_tr('a'));
    assert('ş' == im_tr('ş'));
    assert('\U0001ffff' == im_tr('\U0001ffff'));

    assert(im_tr('ç') < im_tr('d'));
    assert(im_tr('i') >= im_tr('ı'));
    assert(im_tr('ğ') < 'h');
    assert(im_tr('t') >= 'ş');

    assert(im_tr('f') < im_tr('g'));
    assert(im_tr('a') < im_tr('â'));
    assert(im_tr('a') < im_tr('A'));

    // Aksanlı a daha öncedir
    assert(im_tr('d') > 'ā');
    assert(im_tr('ç') > 'ā');

    // Alfabede bulunmayan bütün karakterler daha sonradır
    assert(im_tr('d') < 'Þ');
    assert(im_tr('ç') < 'Þ');
    assert(im_tr('d') > ' ');
    assert(im_tr('ç') > ' ');
    assert(im_tr('d') < '`');
    assert(im_tr('ç') < '`');

    assert(cast(dchar)im_tr('a') == 'a');
    assert(cast(dchar)im_tr('Ğ') == 'Ğ');
    assert(cast(dchar)im_tr('\U0001ffff') == '\U0001ffff');

    int[im_tr] tablo;
    tablo[im_tr('ğ')] = 42;
    tablo[im_tr('z')] = 7;
    assert(tablo[im_tr('ğ')] == 42);

    assert(im_tr('ı').büyüğü == 'I');
    assert(im_tr('I').büyüğü == 'I');
    assert(im_tr('İ').küçüğü == 'i');
    assert(im_tr('i').küçüğü == 'i');

    assert(im_tr('â').küçüğü == 'â');
    assert(im_tr('Â').küçüğü == 'â');
    assert(im_tr('â').büyüğü == 'Â');
    assert(im_tr('Â').büyüğü == 'Â');

    assert(im_tr('â').aksansızı == 'a');
    assert(im_tr('Â').aksansızı == 'A');
    assert(İm!"eng"('ğ').aksansızı == 'g');

    {
        im_tr im;

        im = 'I';
        im = im.küçüğü;
        assert(im == 'ı');
        im = im.büyüğü;
        assert(im == 'I');

        im = 'İ';
        im = im.küçüğü;
        assert(im == 'i');
        im = im.büyüğü;
        assert(im == 'İ');

        im = 'î';
        im = im.büyüğü;
        assert(im == 'Î');
        im = im.küçüğü;
        assert(im == 'î');
    }

    assert(im_tr('i') == im_tr('i'));

    // Farklı alfabe imleri karşılaştırılamıyorlar; güzel...
    assert(!__traits(compiles, im_tr('i') == İm!"eng"('i')));

    // Açıkça Türk alfabesini kullanınca çalışıyor ...
    assert(im_tr('i') == cast(dchar)İm!"eng"('i'));
    assert(im_tr('ı') == cast(dchar)İm!"eng"('ı'));
    // ... veya açıkça İngiliz alfabesini kullanınca çalışıyor
    assert(cast(dchar)im_tr('i') == İm!"eng"('i'));
    assert(cast(dchar)im_tr('ı') == İm!"eng"('ı'));
}

/**
 * Sıra karşılaştırmalarında bakılması gereken iki değeri karşılaştırır.
 *
 * Birincil değer sıfırdan farklıysa, sırayı o belirler; sıfırsa, sırayı
 * belirlemek ikincil değere kalır.
 */
int ikiliSıraKarşılaştırması(int birincil, int ikincil)
{
    return birincil ? birincil : ikincil;
}

unittest
{
    assert(ikiliSıraKarşılaştırması(5, 3) == 5);
    assert(ikiliSıraKarşılaştırması(5, 13) == 5);
    assert(ikiliSıraKarşılaştırması(0, 7) == 7);
}

/**
 * ikiliSıraKarşılaştırması'nın üç değerlisidir
 */
int üçlüSıraKarşılaştırması(int birincil, int ikincil, int üçüncül)
{
    return birincil ? birincil : ikiliSıraKarşılaştırması(ikincil, üçüncül);
}

unittest
{
    assert(üçlüSıraKarşılaştırması(0, 0, 4) == 4);
    assert(üçlüSıraKarşılaştırması(0, 3, 4) == 3);
    assert(üçlüSıraKarşılaştırması(2, 3, 4) == 2);
}