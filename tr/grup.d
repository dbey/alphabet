module tr.grup;

import tr.harf;

import std.string;

/**
 * Bir grup alfabe harfini temsil eder
 */
struct Grup
{
    dstring grup;

    /**
     * Belirtilen harfin çıkarıldığı bir harf grubu döndürür
     *
     * Params:
     *     çıkan = Gruptan çıkarılacak olan harf
     *
     * Returns: Belirtilen harfin çıkarıldığı yeni bir harf grubu
     */
    Grup opBinary(string işleç)(Harf çıkan) const
        if (işleç == "-")
    out (sonuç)
    {
        assert(sonuç.grup.length < grup.length,
               format("%s içinde %s yok", grup, çıkan));
    }
    body
    {
        dstring sonuç;

        foreach (harf; grup) {
            if (harf != çıkan.kod) {
                sonuç ~= harf;
            }
        }

        return Grup(sonuç);
    }

    /**
     * Belirtilen harflerin çıkarıldığı bir harf grubu döndürür
     *
     * Params:
     *     çıkan = Gruptan çıkarılacak olan harfler
     *
     * Returns: Belirtilen harflerin çıkarıldığı yeni bir harf grubu
     */
    Grup opBinary(string işleç)(Grup çıkan) const
        if (işleç == "-")
    {
        Grup sonuç = this;

        foreach (harf; çıkan.grup) {
            sonuç = sonuç - Harf(harf);
        }

        return sonuç;
    }

    /**
     * Başına belirtilen harf eklenmiş bir harf grubu döndürür
     *
     * Params:
     *     eklenen = Grubun başına eklenecek olan harf
     *
     * Returns: Belirtilen harfin baş tarafına eklendiği yeni bir harf grubu
     */
    Grup opBinaryRight(string işleç)(Harf eklenen) const
        if (işleç == "~")
    in
    {
        assert(indexOf(grup, eklenen.kod) == -1,
               format("%s içinde zaten %s var", grup, eklenen));
    }
    body
    {
        return Grup(eklenen.kod ~ grup);
    }

    string toString() const
    {
        return format("%s", grup);
    }
}

unittest
{
    import std.conv;

    auto grup = Grup("abc");
    assert((grup - Harf('b')).grup == "ac");
    assert((Harf('z') ~ grup).grup == "zabc");
    assert(to!string(grup) == "abc");

    /*
     * Sonucunda hata atılması gereken işlemleri denetler
     */
    void hataAtılmalı(string mesaj, void delegate() işlem)
    {
        bool atıldı = false;

        try {
            işlem();

        } catch (Throwable hata) {
            atıldı = true;
        }

        assert(atıldı, "Beklenen hata atılmadı: " ~ mesaj);
    }

    hataAtılmalı("bulunmayan harf çıkartmak",
                 { Grup("abc") - Harf('z');});

    hataAtılmalı("var olan harf eklemek",
                 { Harf('b') ~ Grup("abc"); });
}
